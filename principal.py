from src.data.Articulo import Articulo
from src.data.Cliente import Cliente
from src.data.Fac_cabe import Fac_cabe

def crearSubMenu(obj, alias, callback) :
    while True:
        index = 0
        #foreach
        for item in obj.listar():
            index+=1
            print("Item: " + str(index))
            print(item)
        print("--- Seleccionar una accion ---")
        print("A. Crear " + alias.lower())
        if alias == "Factura":
            print("B. Visualizar " + alias.lower())
        else:
            print("B. Actualizar " + alias.lower())
        print("C. Eliminar " + alias.lower())
        print("D. Volver al menú principal")
        accion = str(input("Seleccione: ")).upper()
        if accion == "D":
            return
        
        if accion == "A":
            inputData = callback()
            print(obj.crear(inputData))
        
        if accion == "B":
            if alias == "Factura":
                id = int(input("Seleccione id de factura a visualizar: "))
                print(obj.visualizar(id))
            else :
                id = int(input("Seleccione id de " + alias.lower() + "a actualizar: "))
                inputData = callback()
                print(obj.actualizar(id, inputData))
        
        if accion == "C":
            id = int(input("Seleccione id de " + alias.lower() + " a eliminar: "))
            print(obj.eliminar(id))
    

# funcionando
def manejarArticulos():
    obj = Articulo()
    def definirInput():
        return {
            "art_nombre": input("Ingresar nombre del producto: "),
            "art_unidad": int(input("Ingresar unidades: ")),
            "art_precio": float(input("Ingresar Precio: ")),
            "art_stock": int(input("Ingresar stock: "))
        }
    crearSubMenu(obj, "Artículo", definirInput)

# funcionando
def manejarClientes():
    obj = Cliente()
    def definirInput():
        return {
            "cli_nombre": input("Ingresar nombre del cliente: "),
            "cli_telefono": int(input("Ingresar telefono: ")),
            "cli_ruc": (input("Ingresar RUC: ")),
            "cli_direccion": (input("Ingresar direccion: "))
        }
    crearSubMenu(obj, "Cliente", definirInput)

# funcionando
def manejarFacturas():
    obj = Fac_cabe()
    def definirInput():
        agregar_mas = True
        objArt = Articulo()
        objCli = Cliente()
        cli = None
        while cli == None:
            idcli = int(input("Ingresar código del cliente: "))
            cli = objCli.encontrar(idcli)
            if cli == None:
                print("El código del cliente \"" + str(idcli) + "\" no es válido, ingrese el código de un cliente registrado")

        itemBase = {
            "fac_fecha": str(input("Ingresar fecha de factura: ")),
            "cli_codigo": int(cli["cli_codigo"]),
            "detalles": [],
            "fac_igv": 0
        }

        while agregar_mas == True:
            print("--INGRESANDO ARTICULOS--")
            codArt = int(input("Ingresar Codigo de articulo: "))
            art = objArt.encontrar(codArt)
            if (art == None) :
                print("El código de articulo \"" + str(codArt) + "\" no es válido, por favor ingrese el código de un articulo que este registrado.")
                continue
            item = {
                "fac_numero": 0,
                "art_codigo": art["art_codigo"],
                "art_cantidad": int(input("Ingresar cantidad: ")),
                "precio": float(art["art_precio"])
            }
            itemBase["detalles"].append(item)
            agregar_mas = input("¿Desesa agregar otro articulo? ").lower() == "si"

        itemBase["fac_igv"] = float(sum(articulo["art_cantidad"] * articulo["precio"] for articulo in itemBase["detalles"]) * 0.18)
        for d in itemBase["detalles"]:
            d.pop("precio")

        return itemBase
    crearSubMenu(obj, "Factura", definirInput)



def menuPrincipal():
    
    continuar = True
    while continuar == True :
        print("----------MENU PRINCIPAL------------")
        print("1. Articulo")
        print("2. Cliente")
        print("3. Factura")
        print("4. Salir")
        opcion = int(input("Seleccione una opcion: "))
        if (opcion == 1) :
            manejarArticulos()
        if (opcion == 2) :
            manejarClientes()
        if (opcion == 3) :
            manejarFacturas()
        if (opcion == 4):
            print("Cerrando aplicación")
            return
        
        continuar = str(input("¿Desea realizar otra operacion?: ")).lower() == "si"
        if continuar == False:
            print("Cerrando aplicación")
        

menuPrincipal()
from ..BD.conexion import dao
class Cliente(dao):

    def __init__(self):
        super().__init__()
    
    def listar(self):
        return super().listar("SELECT * FROM cliente")
    
    def encontrar(self, id):
        return super().encontrar("SELECT cli_codigo, cli_nombre, cli_telefono, cli_ruc, cli_direccion FROM cliente WHERE cli_codigo = %s LIMIT 1", (id))
    
    def crear(self, objData):
        creado = super().crear(
            "INSERT cliente (cli_nombre,cli_telefono,cli_ruc,cli_direccion) VALUES(%s,%s,%s,%s)",
            (objData["cli_nombre"],objData["cli_telefono"],objData["cli_ruc"],objData["cli_direccion"])
        )
        if creado >= 1:
            return "Cliente creado satisfactoriamente"
        return "No se pudo crear el cliente"
    

    def actualizar(self, id,objData):
        actualizado = super().actualizar("UPDATE cliente SET cli_nombre = %s,cli_telefono = %s,cli_ruc = %s,cli_direccion = %s WHERE cli_codigo = %s",(objData["cli_nombre"], objData["cli_telefono"], objData["cli_ruc"], objData["cli_direccion"], id))
        if actualizado >= 1:
            return "Cliente actualizado satisfactoriamente"
        return "No se pudo actualizar el cliente"
    
    def eliminar(self, id):
        eliminado = super().eliminar("DELETE FROM cliente WHERE cli_codigo = %s", (id))
        if eliminado >= 1:
            return "Cliente eliminado satisfactoriamente"
        return "No se pudo eliminar el cliente"
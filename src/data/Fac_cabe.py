from ..BD.conexion import dao
class Fac_cabe(dao):

    def __init__(self):
        super().__init__()
    
    def listar(self):
        items = []
        for i in super().listar("SELECT * FROM fac_cabe"):
            i["fac_fecha"] = i["fac_fecha"].strftime('%d-%m-%Y')
            items.append(i)
        return items

    def crear(self, objData):
        creado = super().crear(
            "INSERT fac_cabe (fac_fecha, cli_codigo, fac_igv) VALUES(%s, %s, %s)",
            (objData["fac_fecha"], objData["cli_codigo"], objData["fac_igv"])
        )
        if creado >= 1:
            self._guardarDetalles(creado, objData)
            return "Factura_cabe creado satisfactoriamente"
        return "No se pudo crear la factura_cabe"
    
    def _guardarDetalles(self, id, objData):
        super().eliminar("DELETE FROM fac_deta WHERE fac_numero = %s", (id))
        items = []
        values = []
        for d in objData["detalles"]:
            d["fac_numero"] = id
            for v in d.values():
                values.append(v)
            items.append(", ".join(["%s" + "" for _ in list(d.values())]))
        sql = "INSERT INTO fac_deta (" + (", ".join(objData["detalles"][0].keys())) + ") VALUES (" + ("), (".join(items)) + ")"
        super().crear(sql, tuple(values))


    def actualizar(self, id, fac_fecha, cli_codigo, fac_igv):
        actualizado = super().actualizar("UPDATE fac_cabe SET fac_fecha = %s, cli_codigo = %s, fac_igv = %s WHERE fac_num = %s", (fac_fecha, cli_codigo, fac_igv, id))
        if actualizado >= 1:
            return "Factura_cabe actualizado satisfactoriamente"
        return "No se pudo actualizar el factura_cabe"

    def eliminar(self, id):
        eliminado = super().eliminar("DELETE FROM fac_cabe WHERE fac_numero = %s", (id))
        if eliminado >= 1:
            return "Factura_cabe eliminado satisfactoriamente"
        return "No se pudo eliminar el factura_cabe"
    
    def visualizar(self, id):
        factura = super().encontrar("SELECT fc.fac_numero AS id, c.cli_nombre AS cliente, fc.fac_fecha AS fecha, fc.fac_igv AS igv FROM fac_cabe fc INNER JOIN cliente c ON fc.cli_codigo = c.cli_codigo where fc.fac_numero = %s LIMIT 1", (id))
        if factura != None:
            factura["fecha"] = factura["fecha"].strftime('%d-%m-%Y')
        return {
            "factura": factura,
            "detalles": super().listar("SELECT fd.fac_numero AS id, a.art_nombre AS articulo, a.art_precio AS precio, fd.art_cantidad AS cantidad FROM fac_deta fd INNER JOIN articulo a ON fd.art_codigo = a.art_codigo where fd.fac_numero = " + str(id))
        }
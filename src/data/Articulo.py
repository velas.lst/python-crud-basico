from ..BD.conexion import dao
class Articulo(dao):

    def __init__(self):
        super().__init__()

    def listar(self):
        return super().listar("SELECT * FROM articulo")
    
    def encontrar(self, id):
        return super().encontrar("SELECT art_codigo, art_nombre, art_unidad, art_precio, art_stock FROM articulo WHERE art_codigo = %s LIMIT 1", (id))

    def crear(self, objData):
        creado = super().crear(
            "INSERT articulo (art_nombre, art_unidad, art_precio, art_stock) VALUES(%s, %s, %s, %s)",
            (objData["art_nombre"], objData["art_unidad"], objData["art_precio"], objData["art_stock"])
        )
        if creado >= 1:
            return "Articulo creado satisfactoriamente"
        return "No se pudo crear el articulo"
    


    def actualizar(self, id, objData):
        actualizado = super().actualizar("UPDATE articulo SET art_nombre = %s, art_unidad = %s, art_precio = %s, art_stock = %s WHERE art_codigo = %s", (objData["art_nombre"], objData["art_unidad"], objData["art_precio"], objData["art_stock"], id))
        if actualizado >= 1:
            return "Articulo actualizado satisfactoriamente"
        return "No se pudo actualizar el articulo"

    def eliminar(self, id):
        eliminado = super().eliminar("DELETE FROM articulo WHERE art_codigo = %s", (id))
        if eliminado >= 1:
            return "Articulo eliminado satisfactoriamente"
        return "No se pudo eliminar el articulo"
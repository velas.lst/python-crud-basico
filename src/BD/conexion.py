import pymysql

class dao():
    host = 'localhost'
    user = 'root' # Cambiar en la presentacion, por las credenciales coorespondientes
    password = 'root' # Cambiar en la presentacion, por las credenciales coorespondientes
    db = 'bdventas'
    cursorclass = pymysql.cursors.DictCursor

    _conexion = None


    def __init__(self):
        try:
            self._conexion = pymysql.connect(
                host = self.host,
                user = self.user,
                password = self.password,
                db = self.db,
                cursorclass =self.cursorclass
            )
        except:
            print("Ocurrio un error al conectar con la base de datos")

    def listar(self, sql):
        registros = ()
        with self._conexion.cursor() as sentencia:
            sentencia.execute(sql)
            registros = sentencia.fetchall()
        self._conexion.commit()
        return registros
    
    def encontrar(self, sql, args):
        registros = ()
        with self._conexion.cursor() as sentencia:
            sentencia.execute(sql, args)
            registros = sentencia.fetchone()
        self._conexion.commit()
        return registros

    def crear(self, sql, args):
        lrid = 0
        with self._conexion.cursor() as sentencia:
            sentencia.execute(sql,args)
            lrid = sentencia.lastrowid
        self._conexion.commit()
        return lrid

    def actualizar(self, sql, args):
        afectados = 0
        with self._conexion.cursor() as sentencia:
            afectados = sentencia.execute(sql, args)
        self._conexion.commit()
        return afectados

    def eliminar(self, sql, args):
        with self._conexion.cursor() as sentencia:
            afectados = sentencia.execute(sql,args)
        self._conexion.commit()
        return afectados
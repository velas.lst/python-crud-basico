create database bdventas;
use bdventas;
create table cliente (
cli_codigo INT primary key auto_increment,
cli_nombre varchar(20) not null,
cli_telefono varchar(9),
cli_ruc varchar(11) not null,
cli_direccion varchar(30)
);
create table articulo (
art_codigo int primary key auto_increment,
art_nombre varchar(20) not null,
art_unidad int,
art_precio decimal(8,2),
art_stock int
);
create table fac_cabe (
fac_numero int primary key auto_increment,
fac_fecha date,
cli_codigo int,
fac_igv decimal(8,2)
);
create table fac_deta (
fac_deta_id int primary key auto_increment,
fac_numero int,
art_codigo int,
art_cantidad int
);
alter table fac_cabe add constraint fk_cli_codigo foreign key (cli_codigo) references cliente(cli_codigo);
alter table fac_deta add constraint fk_fac_numero foreign key (fac_numero) references fac_cabe(fac_numero);
alter table fac_deta add constraint fk_art_codigo foreign key (art_codigo) references articulo(art_codigo);